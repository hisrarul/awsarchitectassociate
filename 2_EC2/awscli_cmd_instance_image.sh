#Before executing this script check permission of IAM user or IAM role configured on this AWS CLI

# Enter the correct Instance Id and appropriate name with description.
aws ec2 create-image --instance-id i-1234567890abcdef0 --name "My server" --description "An AMI for my server"

# Start instance using aws cli
aws ec2 start-instances --instance-ids i-1234567890abcdef0

# Stop instance
aws ec2 stop-instances --instance-ids i-1234567890abcdef0

# Describe instance
aws ec2 describe-instances --instance-ids i-1234567890abcdef0

# Filter the output using query
aws ec2 describe-instances --query "Reservations[*].Instances[*].["InstanceType"]"

aws ec2 describe-instances --query "Reservations[*].Instances[*].[ImageId,Tags[*]]"

# Filter instance
aws ec2 describe-instances --filters "Name=tag-key,Values=Owner"

#Output Code appeared in json (Can be ignored)
0 : pending
16 : running
32 : shutting-down
48 : terminated
64 : stopping
80 : stopped

# This document has created by Israrul Haque